(function() {
  var banner = $('.banner ul')
    , testimonials = $('.testimonials .content ul');
  $('select').uniform({wrapperClass: "ssa-theme"});

  if(banner.length === 1) {
    banner.cycle({
      fx:     'scrollLeft',
      pager:  '.banner-pages',
      next:   '#next',
      timeout: 0,
      prev:   '#prev'
    });
  }

  if(testimonials.length === 1) {
    testimonials.cycle({
      fx:     'fade',
      pager:  '.testimonial-pages'
    });
  }

  $('.yes').on('click', function(event){
    event.preventDefault();
    var there = $(this);
    var container = there.parent('.helpful');

    container.html('Obrigado pelo Feedback!');
  });

  $('.no').on('click', function(event){
    event.preventDefault();
    var there = $(this);
    var container = there.parent('.helpful');

    container.html('Obrigado pelo Feedback!');
  });
})();
