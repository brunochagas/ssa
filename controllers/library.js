module.exports = function(app) {
  var LibraryController = {
    index: function(req, res) {
      var fs = require('fs')
        , xml2js = require('xml2js')
        , parser = new xml2js.Parser({xmlns: true, explicitCharkey: true, explicitArray: false, mergeAttrs: true});

      fs.readFile('./xml/bibliotecaSSA.xml', function(err, data) {
        parser.parseString(data, function (err, result) {
          var books = result.books.book;
          res.render('library/books', { current: 'library', books: books });
        });
      });
    },
  };
  return LibraryController;
};

