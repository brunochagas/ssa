module.exports = function(app) {
  var PagesController = {
    index: function(req, res) {
      res.render('pages/index', { current: 'index', message: false, errors: {} });
    },
    about: function(req, res) {
      res.render('pages/quem-somos', { current: 'about' });
    },
    product: function(req, res){
      res.render('pages/produto', { current: 'product' });
    },
    costs: function(req, res){
      res.render('pages/custos', { current: 'costs' });
    },
    platform: function(req, res){
      res.render('pages/plataforma', { current: 'platform' });
    },
    disciplines: function(req, res){
      res.render('pages/disciplinas', { current: 'disciplines' });
    },
    how_to_buy: function(req, res){
      res.render('pages/como-adquirir', { current: 'how-to' });
    },
    contact: function(req, res){
      res.render('pages/contato', { current: 'contact', message: false, errors: {} });
    },
    send_apointment: function(req, res){
      req.assert('email', 'Email não pode ficar em branco.').notEmpty();
      req.assert('email', 'Formato de email não é válido.').isEmail();
      req.assert('name', 'Nome não pode ficar em branco.').notEmpty();
      req.assert('message', 'Mensagem não pode ficar em branco.').notEmpty();

      var errors = req.validationErrors();

      if(!errors) {
        var mailOpts, smtpTrans;
        //Setup Nodemailer transport, I chose gmail. Create an application-specific password to avoid problems.
        smtpTrans = nodemailer.createTransport('SMTP', {
          service: 'Gmail',
          auth: {
              user: process.env['EMAIL_USER'],
              pass: process.env['EMAIL_PASS']
          }
        });
        //Mail options
        mailOpts = {
          from: req.body.name + ' &lt;' + req.body.email + '&gt;', //grab form data from the request body object
          to: process.env['EMAIL'],
          subject: 'Contato do site',
          html: "<h2>Name: " + req.body.name + "</h2>" +
                "<h3>Email: " + req.body.email + "</h3>" +
                "----------------------------------------------------------\n" +
                "<h4>Mensagem: </h4>" +
                "<p>" + req.body.message + "<p>"
        };

        smtpTrans.sendMail(mailOpts, function (error, response) {
          //Email sent
          if (!error) {
            res.render('pages/index', { current: 'index', message: true, errors: false });
          }
        });
      } else {
        res.render('pages/index', { current: 'index', message: false, errors: errors });
      }
    },
    send_email: function(req, res) {
      req.assert('email', 'Email não pode ficar em branco.').notEmpty();
      req.assert('email', 'Formato de email não é válido.').isEmail();
      req.assert('name', 'Nome não pode ficar em branco.').notEmpty();
      req.assert('message', 'Mensagem não pode ficar em branco.').notEmpty();

      var errors = req.validationErrors();

      if(!errors) {
        var mailOpts, smtpTrans;
        //Setup Nodemailer transport, I chose gmail. Create an application-specific password to avoid problems.
        smtpTrans = nodemailer.createTransport('SMTP', {
          service: 'Gmail',
          auth: {
              user: process.env['EMAIL_USER'],
              pass: process.env['EMAIL_PASS']
          }
        });
        //Mail options
        mailOpts = {
          from: req.body.name + ' &lt;' + req.body.email + '&gt;', //grab form data from the request body object
          to: process.env['EMAIL'],
          subject: 'Contato do site',
          html: "<h2>Name: " + req.body.name + "</h2>" +
                "<h3>Telefone: " + req.body.ddd + '-' + req.body.phone + "</h3>" +
                "<h4>Subject: " + req.body.subject + "</h4>" +
                "----------------------------------------------------------\n" +
                "<h4>Mensagem: </h4>" +
                "<p>" + req.body.message + "<p>"
        };

        smtpTrans.sendMail(mailOpts, function (error, response) {
          //Email not sent
          if (!error) {
            res.render('pages/contato', { current: 'contact', message: true, errors: false });
          }
        });
      } else {
        res.render('pages/contato', { current: 'contact', message: false, errors: errors });
      }
    },
    faq: function(req, res){
      res.render('pages/faq', { current: 'faq' });
    },
    faq_question: function(req, res){
      res.render('pages/faq-pergunta', { current: 'faq' });
    }
  };
  return PagesController;
};
