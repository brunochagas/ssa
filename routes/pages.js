module.exports = function(app) {
  var pages = app.controllers.pages
    , nodemailer = require('nodemailer');

  app.get('/', pages.index);
  app.get('/quem-somos', pages.about);
  app.get('/produto', pages.product);
  app.get('/custos', pages.costs);
  app.get('/plataforma', pages.platform);
  app.get('/disciplinas', pages.disciplines);
  app.get('/como-adquirir', pages.how_to_buy);
  app.get('/contato', pages.contact);
  app.post('/contato', pages.send_email);
  app.get('/faq', pages.faq);
  app.get('/faq-pergunta', pages.faq_question);
  app.post('/agendamento', pages.send_apointment);
  app.post('/faq-enviar-pergunta', function(req, res) {
     var mailOpts, smtpTrans;
    //Setup Nodemailer transport, I chose gmail. Create an application-specific password to avoid problems.
    smtpTrans = nodemailer.createTransport('SMTP', {
      service: 'Gmail',
      auth: {
          user: process.env['EMAIL_USER'],
          pass: process.env['EMAIL_PASS']
      }
    });
    //Mail options
    mailOpts = {
      from: req.body.name + ' &lt;' + req.body.email + '&gt;', //grab form data from the request body object
      to: process.env['EMAIL_TO'],
      subject: 'Contato do site',
      html: "<h2>Name: " + req.body.name + "</h2>" +
            "<h3>Email: " + req.body.email + "</h3>" +
            "----------------------------------------------------------\n" +
            "<h4>Question: </h4>" +
            "<p>" + req.body.question + "<p>"
    };

    smtpTrans.sendMail(mailOpts, function (error, response) {
      //Email not sent
      if (error) {
        res.render('pages/faq-pergunta', { current: 'faq', err: true, message: 'Erro ao enviar mensagem, tente novamente mais tarde.' })
      }
      //Yay!! Email sent
      else {
        res.render('pages/faq-pergunta', { current: 'faq', message: 'Mensagem enviada com sucesso!' })
      }
    });
  });
};

