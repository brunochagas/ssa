var express = require('express')
  , expressValidator = require('express-validator')
  , engine = require('ejs-locals')
  , app = express()
  , load = require('express-load')
  , server = require('http').createServer(app)
  , error = require('./middleware/error')
  , port = process.env.PORT || 5000
;

app.engine('ejs', engine);
app.use(express.logger());
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.set('port', process.env.PORT || 5000);
app.use(express.static(__dirname + '/public'));
app.use(express.bodyParser());
app.use(expressValidator);
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(__dirname + '/public'));
app.use(error.notFound);

load('models')
  .then('controllers')
  .then('routes')
  .into(app);

server.listen(port, function(){
  console.log("Server listen" + port);
});

module.exports = app;

