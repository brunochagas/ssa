module.exports = function(grunt) {
	grunt.initConfig({
    dir: {
      static_sass: 'sass/',
      static_css: 'public/stylesheets/'
    },

    sass: {
      dist: {
        options: {
          style: 'compressed'
        },
        expand: true,
        cwd: '<%= dir.static_sass %>',
        src: ['*.sass'],
        dest: '<%= dir.static_css %>',
        ext: '.css'
      }
    },

    watch: {
      css: {
        files: ['<%= dir.static_sass %>**/*.sass'],
        tasks: ['sass']
      }
    }
	});

	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.registerTask('default',['watch']);
}
